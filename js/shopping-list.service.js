
(function () {
    'use strict';

    angular.module("shoppingApp")
        .service("shoppingListService", function () {
           var ingredients = [
                {
                    id: 1,
                    name: "Apples",
                    amount: 10
                },
                {
                    id: 2,
                    name: "Tomatos",
                    amount: 15
                }
               ];

            var service = {  
                
                getIngredients: function () {                                       

                   return ingredients.slice();
                },  
                
                getIngredientById: function(id){
                   return (ingredients.filter(rec => rec.id === id)).slice();
                },

                updateIngredient: function (editMode, ingredient){

                    if(editMode){
                        ingredients.forEach(rec => {
                            if(rec.id === ingredient.id){
                                rec.name = ingredient.name;
                                rec.amount = ingredient.amount;
                            }
                          });
                    }
                    else{
                        ingredients.push({id:Math.floor(Math.random() * 90 + 10), name:ingredient.name, amount: ingredient.amount});
                    }
                },

                deleteIngredient: function(id){
                    ingredients = ingredients.filter(rec => rec.id !== id);                    
                }
            };
            return service;
        });
})();


