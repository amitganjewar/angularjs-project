(function () {
    angular.module("shoppingApp")
        .component("shoppingEdit", {
            bindings: {
                ingredienttoedit: '<', //this is to get the ingredient to be edited from parant component
                onUpdate: '&'  // this is a event binding to notify the parant component back
            },
            templateUrl: "js/shopping-edit/shopping-edit.component.html",
            controller: function ShoppingEditController() {
                
                // lifecycle hook to set the defaults
                this.$onInit = function () {
                    this.setDefaults();                  
                };

                // life cycle hook to set ingredient to be updated once ingredient is selected from parant component.
                this.$onChanges = function (changes) {
                    if (changes.ingredienttoedit.currentValue.id!=null) {
                        console.log(changes.ingredienttoedit);
                        this.ingredientToEdit = changes.ingredienttoedit.currentValue;
                        this.editMode = true;  
                        this.showUpdateButtons = true;                      
                    }
                };

                // on update or adding new ingredient notify the parant component about the action to take.
                this.onSubmit = function () {
                    this.showUpdateButtons = false;   
                    if (this.editMode) {
                        this.notifyParent('update', this.ingredientToEdit);                        
                    }
                    else {
                        if (this.ingredientToEdit.name !== null && this.ingredientToEdit.amount !== null) {                          
                            this.notifyParent('add', this.ingredientToEdit);
                        }
                    }   
                    this.setDefaults();
                                   
                };

                // setup default values to ingredeint and edit mode
                this.setDefaults = function () {
                    this.editMode = false;
                    this.ingredientToEdit = {
                        id: null,
                        name: null,
                        amount: null
                    };
                };

                // on deleting ingredient notify paranet component to take action.
                this.onDelete = function () {
                    this.showUpdateButtons = false;                     
                    this.notifyParent('delete', this.ingredientToEdit);
                    this.setDefaults();
                };

                //clearing the form values
                this.onClear = function () {
                    this.setDefaults();
                };

                // actual method which emmits the event back up to the parant component to notify the change.
                this.notifyParent = function (actionType, ingredientImpacted) {
                    this.onUpdate({
                        $event: {
                            action:actionType,
                            ingredient:ingredientImpacted
                        }
                    });
                };
            }
        })


})();