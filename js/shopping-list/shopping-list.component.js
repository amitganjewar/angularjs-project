(function () {
    angular.module("shoppingApp")
        .component("shoppingList", {
            templateUrl: "js/shopping-list/shopping-list.component.html",
            controller: function ShoppingListController(shoppingListService) {

                // lifecycle hook to setup the initial values.
                this.$onInit = function () {
                    this.setDefaults();
                };

                // set the values to defaults
                this.setDefaults = function(){
                    this.ingredientToEdit = {
                        id: null,
                        name:null,
                        amont:null
                    };
                    this.ingredients = shoppingListService.getIngredients();
                }

                // on selecting any ingredient pass the same down to child component in order to update the same.
                this.onEditItem = function (inputIngredient) {
                    this.ingredientToEdit = inputIngredient;
                };

                // method passed to child component through event binding and acting once emmited by child component.
                this.onUpdateIngredient = function (event) {  
                             
                    switch(event.action) {
                        case 'add':
                            shoppingListService.updateIngredient(false, event.ingredient);
                            break;
                        case 'update':
                            shoppingListService.updateIngredient(true, event.ingredient);
                            break;
                        case 'delete':
                            shoppingListService.deleteIngredient(event.ingredient.id);
                            break;
                        default:
                            this.ingredients = shoppingListService.getIngredients();
                    }                  
                    this.setDefaults();
                };
            }
        })
})();  